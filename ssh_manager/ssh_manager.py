#!/usr/bin/env python3
import os
import re

targets=[]
def menu():
    with open('address.txt','r') as f:
        machines=list(f)
        #n=str(sum(1 for l in open('address.txt', 'r')))
        n=str(sum(1 for l in machines))
        print("List of available machines :\n")
        for line in machines:
            line=line.strip('\n')
            print (line)

    print("What do you want: \n")
    print("1) upload script ")
    print("2) run command ")
    case=input()
    print("Select machine numbers (1-"+ n +") separated by commas,or 'all': ")
    number=input()


    if number.strip().lower() == 'all':
        global targets
        targets=machines[:]
    else:
        for nb in number.split(','):
            if nb.find('-') != -1:
                (sx,sy) = nb.split('-')
                if sx.isdigit() and sy.isdigit():
                    x=int(sx)
                    y=int(sy)+1
                    for v in range(x,y):
                        targets.append(machines[v-1])
            elif not nb.isdigit() and nb.strip() !='':
                print("Input Error!!!")
            elif nb != '':
                targets.append(machines[int(nb)-1])


    if case=='1':
        print("Enter script name: ")
        name=input()
        upload_script(name,targets)

    if case=='2':
        print("Enter command: ")
        cmd1=input()
        execute_command(cmd1,targets)



def upload_script(script_name,spisok):
    for i in spisok:
        i=i.strip("\n")
        #print("scp "+script_name+" user@"+i+":/home")
        os.system("scp "+script_name+" user@"+i+":/home")



def execute_command(command,spisok):
    for i in spisok:
        i=i.strip("\n")
        #print('ssh user@'+i+' '+command)
        os.system('ssh user@'+i+' '+command)


menu()

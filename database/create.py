import pymysql
import logging

logging.basicConfig(filename='logger.log',
                    format='%(filename)-12s[LINE:%(lineno)d] %(levelname)-8s %(message)s %(asctime)s ',
                    level=logging.INFO)


def db_connect():
    try:
        connect = pymysql.connect(host='87.103.243.110',
                                  user='dev_life_user',
                                  password='PINLOX!@#',
                                  db='insta_like',
                                  cursorclass=pymysql.cursors.DictCursor)
        return connect, connect.cursor()
    except:
        logging.error('Fatal error: connect database')
        return -1, -1


def create_table_user():
    connect, current_connect = db_connect()

    sql = "CREATE TABLE USER_INFO (" \
          "ID int(10) NOT NULL AUTO_INCREMENT," \
          "Name varchar(256) NOT NULL," \
          "Subscribe int(12) NOT NULL," \
          "Status int(1) NOT NULL," \
          "Rating int(10) NOT NULL," \
          "PRIMARY KEY (id)" \
          ") " \
          "ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;"
    print(sql)
    try:
        current_connect.execute(sql)
        current_connect.close()
    except:
        return


def create_table_picture():
    connect, current_connect = db_connect()

    sql = "CREATE TABLE PICTURE (" \
          "ID int(10) NOT NULL AUTO_INCREMENT," \
          "ID_USER varchar(256) NOT NULL," \
          "Photo varchar(256) NOT NULL," \
          "Like int(10) NOT NULL," \
          "Hashtags varchar(256) NOT NULL," \
          "Data_load varchar(32) NOT NULL," \
          "Data_current varchar(32) NOT NULL," \
          "rating int(10) NOT NULL," \
          "PRIMARY KEY (id)" \
          ") " \
          "ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;"
    print(sql)
    try:
        current_connect.execute(sql)
        current_connect.close()
    except:
        return


def create_table_hashtags():
    connect, current_connect = db_connect()

    sql = "CREATE TABLE HASHTAGS (" \
          "ID int(10) NOT NULL AUTO_INCREMENT," \
          "Hashtags varchar(256) NOT NULL," \
          "Public int(10) NOT NULL," \
          "Rating int(10) NOT NULL," \
          "PRIMARY KEY (id)" \
          ") " \
          "ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;"
    print(sql)
    try:
        current_connect.execute(sql)
        current_connect.close()
    except:
        return


def create_table_hashtags_top():
    connect, current_connect = db_connect()

    sql = "CREATE TABLE HASHTAGS_TOP (" \
          "ID int(10) NOT NULL AUTO_INCREMENT," \
          "ID_HASHTAG int(10) NOT NULL," \
          "Photo varchar(256) NOT NULL," \
          "Like int(10) NOT NULL," \
          "Data_load varchar(32) NOT NULL," \
          "Data_current varchar(32) NOT NULL," \
          "rating int(10) NOT NULL," \
          "PRIMARY KEY (id)" \
          ") " \
          "ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;"
    print(sql)
    try:
        current_connect.execute(sql)
        current_connect.close()
    except:
        return

def create_table_user_link():
    connect, current_connect = db_connect()

    sql = "CREATE TABLE USER_LINK(" \
          "ID int(10) NOT NULL AUTO_INCREMENT," \
          "NAME varchar(64) NOT NULL," \
          "ID_USER varchar(16) NOT NULL," \
          "PRIMARY KEY (id)" \
          ") " \
          "ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;"
    print(sql)
    try:
        current_connect.execute(sql)
        current_connect.close()
    except:
        return


if __name__ == '__main__':
    create_table_user()
    #create_table_picture()
    #create_table_hashtags()
    #create_table_hashtags_top()
    #create_table_user_link()

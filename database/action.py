import logging
from database.create import db_connect
logging.basicConfig(filename='logger.log',
                    format='%(filename)-12s[LINE:%(lineno)d] %(levelname)-8s %(message)s %(asctime)s ',
                    level=logging.INFO)


def execute_sql(sql):
    connect, current_connect = db_connect()
    try:
        #print(sql)
        current_connect.execute(sql)
        connect.commit()
        result = current_connect.fetchall()
        connect.close()
        return result
    except:
        return


def get_all_users():
    sql = "SELECT * FROM USER_LINK"
    return execute_sql(sql)


def add_user_link(user_id, user_name):
    sql = "SELECT * FROM USER_LINK WHERE ID_USER = {}".format(user_id)
    result = execute_sql(sql)
    if len(result) == 0:
        return insert_user_link(user_id, user_name)


def insert_user_link(user_id, user_name):
    sql = "INSERT INTO USER_LINK VALUES(null, \"{}\", \"{}\")".format(user_name, user_id)
    execute_sql(sql)


def add_user_info(user_info):
    sql = "SELECT * FROM USER_INFO WHERE Name = \"{}\"".format(user_info[0])
    result = execute_sql(sql)
    if len(result) == 0:
        return insert_user_info(user_info)


def insert_user_info(user_info):
    sql = "INSERT INTO USER_INFO VALUES(null, \"{}\", {}, {}, 0)".format(user_info[0], user_info[1], user_info[2])
    execute_sql(sql)


def add_photo(*user_info):
    sql = "INSERT INTO USER_LINK VALUES(null, \"{}\", \"{}\")".format(user_info[0], user_info[1])
    execute_sql(sql)


def get_user(user_id):
    sql = "SELECT name FROM USER_LINK WHERE id = {}".format(user_id)
    result = execute_sql(sql)
    if len(result) != 0:
        return result[0]['name']
    else:
        return 'end_user'


def count_user():
    sql = "SELECT COUNT(*) FROM USER_LINK"
    return execute_sql(sql)[0]['COUNT(*)']
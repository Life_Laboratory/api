import requests
import re
import json
import time
import threading


class Instagram:
    token = ''
    mid = ''
    session = ''
    next_user = ''
    id_user = 0
    data_user = []
    min = 1
    max = 2
    user_name = ''
    user_info = ()
    status_private = "False"

    def __init__(self, *args):
        if len(args) == 3:
            self.user_name = args[0]
            self.min = args[1]
            self.max = args[2]
        elif len(args) == 2:
            self.user_name = args[0]
            self.max = args[1]
        elif len(args) == 1:
            self.user_name = args[0]
            #self.get_max_followed()
        data = requests.get('https://instagram.com')
        self.token, self.mid = self.__get_token_and_mid(data.headers['set-cookie'])
        headers = {'Host': 'www.instagram.com',
                   'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) '
                                 'Gecko/20100101 Firefox/54.0',
                   'Accept': '*/*',
                   'Accept-Language': 'en-US,en;q=0.5',
                   'Accept-Encoding': 'gzip, deflate, br',
                   'X-CSRFToken': self.token,
                   'X-Instagram-AJAX': '1',
                   'Content-Type': 'application/x-www-form-urlencoded',
                   'X-Requested-With': 'XMLHttpRequest',
                   'Referer': 'https://www.instagram.com/',
                   'Content-Length': '44',
                   'Cookie': 'mid={}; '
                             'rur=ATN; '
                             'ig_vw=1301; '
                             'ig_pr=1; '
                             'ig_dau_dismiss=1499402809485; '
                             'csrftoken={}'.format(self.mid, self.token),
                   'Connection': 'keep-alive'}
        data_auth = {'username': 'life@nskctf.ru',
                     'password': 'pinlox123'}
        data = requests.post('https://www.instagram.com/accounts/login/ajax/',
                             headers=headers,
                             data=data_auth)
        self.token = self.__get_token(data.headers['Set-Cookie'])
        self.session = self.__get_sessionid(data.headers['Set-Cookie'])
        self.get_starter_page()
        self.get_friends_user()
        if len(args) == 1:
            self.get_max_followed()

    def __iter__(self):
        return self

    def __next__(self):
        if self.min <= self.max:
            self.min += 1
            self.user_info = self.data_user
            self.get_next_friend_user()
            return self.user_info
        else:
            raise StopIteration()

    def get_starter_page(self):
        return requests.get('https://instagram.com/', headers=self.__get_headers()).text

    def get_user(self, user):
        data = requests.get('https://instagram.com/{}/?__a=1'.format(user), headers=self.__get_headers()).text
        self.status_private = re.search(r'"is_private": (true|false)', data).group(1)
        return data

    def get_status_private(self):
        return self.status_private

    def get_max_followed(self):
        self.max = json.loads(self.get_user(self.user_name))['user']['followed_by']['count']
        return self.max

    def get_friends_user(self):
        self.id_user = json.loads(self.get_user(self.user_name))['user']['id']
        headers = self.__get_headers()
        headers['Referer'] = 'https://www.instagram.com/{}'.format(self.user_name)
        url = 'https://www.instagram.com/graphql/query/?query_id=17851374694183129&' \
              'variables={"id":"'+str(self.id_user)+'","first":'+str(self.min)+'}'
        while True:
            data = json.loads(requests.get(url, headers=headers).text)
            if data['status'] == "fail":
                time.sleep(10)
            else:
                break
        self.next_user = data['data']['user']['edge_followed_by']['page_info']['end_cursor']
        self.data_user = data['data']['user']['edge_followed_by']['edges']
        self.user_info = self.data_user#(self.data_user[len(self.data_user)-1]['node']['id'],
                          #self.data_user[len(self.data_user)-1]['node']['username'])

    def get_next_friend_user(self):
        headers = self.__get_headers()
        headers['Referer'] = 'https://www.instagram.com/{}'.format(self.user_name)
        url = 'https://www.instagram.com/graphql/query/?query_id=17851374694183129&' \
              'variables={"id":"'+str(self.id_user)+'","first":1000,"after":"'+str(self.next_user)+'"}'
        while True:
            data = json.loads(requests.get(url, headers=headers).text)
            if data['status'] == "fail":
                time.sleep(10)
            else:
                break
        self.next_user = data['data']['user']['edge_followed_by']['page_info']['end_cursor']
        self.data_user = data['data']['user']['edge_followed_by']['edges']
        return self.data_user

    def get_user_json(self):
        data = requests.get('http://instagram.com/{}/?__a=1'.format(self.user_name)).text.encode('charmap').decode('utf-8')
        return json.loads(data)

    def __get_token_and_mid(self, data):
        return self.__get_token(data), self.__get_mid(data)

    def __get_token(self, data):
        return re.search(r'csrftoken=([a-zA-Z0-9]+)', data).group(1)

    def __get_mid(self, data):
        return re.search(r'mid=([a-zA-Z0-9\-_]+)', data).group(1)

    def __get_sessionid(self, data):
        session = re.search(r'sessionid=([a-zA-Z0-9\-\_\%\.]+);', data).group(1)
        session = re.sub(r'%3A', ':', session)
        session = re.sub(r'%7B', '{', session)
        session = re.sub(r'%22', '"', session)
        session = re.sub(r'%7D', '}', session)
        session = re.sub(r'%2C', ',', session)
        return session

    def __get_headers(self):
        return {'Host': 'www.instagram.com',
                   'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:54.0) '
                                 'Gecko/20100101 Firefox/54.0',
                   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                   'Accept-Language': 'en-US,en;q=0.5',
                   'Accept-Encoding': 'gzip, deflate, br',
                   'Cookie': 'mid={}; '
                             'ds_user_id=5591935970; '
                             'sessionid={};'
                             'rur=FRC; '
                             'ig_vw=1301; '
                             'ig_pr=1; '
                             'ig_dau_dismiss=1499788179227'.format(self.mid, self.session),
                   'Connection': 'keep-alive',
                   'Upgrade-Insecure-Requests': '1',
                   'Cache-Control': 'max-age=0'
                   }

    def get_sessionid(self):
        return self.session

    def set_user(self, name):
        self.user_name = name
        self.get_friends_user()


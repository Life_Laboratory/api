import requests
import re
import json
import time
import threading

class User:
    id_user = 0
    data_user = []
    user_name = ''
    status_private = "False"

    def __init__(self, user_name):
        self.user_name = user_name
        self.get_user()
        self.get_max_followed()

    def get_user(self):
        data = requests.get('https://instagram.com/{}/?__a=1'.format(self.user_name)).text
        self.status_private = re.search(r'"is_private": (true|false)', data).group(1)
        return data

    def get_status_private(self):
        return self.status_private

    def get_max_followed(self):
        self.max = json.loads(self.get_user())['user']['followed_by']['count']

    def get_followed(self):
        return self.max


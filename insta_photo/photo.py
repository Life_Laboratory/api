# -*- coding: utf-8 -*-

import requests
import re
import json
import time
import datetime


def get_user_json(user_name):
    data = requests.get('http://instagram.com/{}/?__a=1'.format(user_name)).text.encode('charmap').decode('utf-8')
    print(data)
    #data = re.search(r'window\._sharedData = ([a-zA-Z0-9 _\-=+!?@#$%^&*(){}":,./\\[\]]+);</script>', data)
    return json.loads(data)


def get_user_info(user_name, data, i=0):
    #data = get_user_json(user_name)
    answer = {
        'username': user_name,
        'user_id': data['user']['id'],
        'followed_by': data['user']['followed_by']['count'],
        'rating': 0,
        'node': {

        }
    }
    for photo in data['user']['media']['nodes']:
        print(photo)
        if photo['is_video'] is False:
            answer['node'][i] = {
                'caption': photo.get('caption'),
                'likes': photo['likes']['count'],
                'comments': photo['comments']['count'],
                'data_upload': photo['date'],
                'data_current': time.time(),
                'src': photo['code']
            }
            i += 1
    return answer


def get_picture_info(username):
    data = get_user_info(username)
    answer = {
        'username': username,
        'user_id': data['user_id'],
    }
    for i in data['node']:
        hashtags0 = re.findall(r'(#[a-zA-Z0-9а-яА-я_*\-=+!@#$%^&]+)', data['node'][i]['caption'])
        answer[i] = {
            'photo': data['node'][i]['src'],
            'likes': data['node'][i]['likes'],
            'hashtags': hashtags0,
            'data_load': data['node'][i]['data_upload'],
            'data_current': time.time()
        }
    return answer


def get_userphoto_info(username):
    data = get_user_info(username)
    answer = {
        'username': username,
        'user_id': data['user_id'],
        'rating': 0,
    }
    hashtag = ['test', ]
    flag = 0
    fl = 0
    for i in data['node']:
        hashtags0 = re.findall(r'(\#[a-zA-Z0-9а-яА-я\_\*\-\=\+\!\@\#\$\%\^\&]+)', data['node'][i]['caption'])
        if hashtags0 != []:
            for i in range(len(hashtag)):
                if hashtag[i] == hashtags0[0]:
                    flag = 1
            if flag == 0:
                if fl == 0:
                    hashtag[i] = hashtags0[0]
                    fl = 1
                else:
                    hashtag.append(hashtags0[0])
            flag = 0
    answer['hashtags'] = hashtag
    return answer

#print (get_userphoto_info('instagram'))
#print (get_picture_info('urgantcom'))
